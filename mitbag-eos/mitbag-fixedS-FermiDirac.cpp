/* 
## Equation of State solver for the MIT Bag Model with fixed Entropy and Fermi-Dirac statistics 
## by Tiago Nunes da Silva, 2017 
## This code depends on the GNU Scientific Library (GSL), see: https://www.gnu.org/software/gsl/
## Compile and link against GSL, e.g.: 
## g++ mitbag-fixedT-FD.cpp -o mitbag-fixedS-FD -std=c++14 -Wall -lgsl -lgslcblas
*/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <vector>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>

using namespace std;

// const integration parameters
const double lower_limit = 0.0; /* lower limit a */
const double abs_error = 1.0e-8;  /* to avoid round-off problems */
const double rel_error = 1.0e-8;  /* the result will usually be much better */

// conversion factors
const double hc = 197.326;
const double rm = 939.0;

// masses and charges
const double mquarks[3] = { 5.0/rm, 5.0/rm, 150.0/rm } ;// masses for u,d,s quarks
const double cquarks[3] = { 2.0/3, -1.0/3, -1.0/3} ;
const double mleptons[2] = { 0.511/rm, 105.66/rm }; // masses for e, muon

// other constants
const double piSquared = 9.8696044011;

//*********************************************************************//

// generic integral solver
// utilizes the GSL library implementation of the Gauss-Kronrod quadrature method
// and solves the integral of a given integrand from 'lower_limit'  to infinity.
// returns the integral result.
// Expects a 'gsl_function' type integrand, 
// see: https://www.gnu.org/software/gsl/manual/html_node/Providing-the-function-to-solve.html
double solveIntegral(double (func)(double, void *), void *parametersPointer)
{
  gsl_integration_workspace *work_ptr = gsl_integration_workspace_alloc (1000);

  double result;    /* the result from the integration */
  double error;     /* the estimated error from the integration */

  gsl_function My_function;
  
  My_function.function = func;
  My_function.params = parametersPointer;

  gsl_integration_qagiu (&My_function, lower_limit, abs_error, rel_error, 1000, work_ptr, &result, &error);

  return result;
}

//*********************************************************************//

// calculates the Fermi-Dirac distribution value for given k, mass, chemPot and temperature
double fermiDirac(double k, double mass, double chemPot, double temperature)
{
  double energy = sqrt(k*k + mass*mass);

  return 1/(1+exp((energy - chemPot)/temperature)) - 1/(1+exp((energy + chemPot)/temperature));
}

// Define parameters for writing the integrands in 'gsl_function' type
struct densityParams {
  double mass; 
  double chemPot;
  double temperature; 
};

// general form for density integrand
double density (double x, void *p)
{
  struct densityParams * params = (struct densityParams *)p;
  double mass = (params->mass);
  double chemPot = (params->chemPot);
  double temperature = (params->temperature);

  return x*x*fermiDirac(x, mass, chemPot, temperature)/piSquared;
}

// general form for energy integrand
double energy (double x, void *p)
{
  struct densityParams * params = (struct densityParams *)p;
  double mass = (params->mass);
  double chemPot = (params->chemPot);
  double temperature = (params->temperature);

  return x*x*sqrt(x*x + mass*mass)*fermiDirac(x, mass, chemPot, temperature)/piSquared;
}

// general form for energy integrand
double pressure (double x, void *p)
{
  struct densityParams * params = (struct densityParams *)p;
  double mass = (params->mass);
  double chemPot = (params->chemPot);
  double temperature = (params->temperature);

  return (pow(x,4)/sqrt(x*x + mass*mass))*fermiDirac(x, mass, chemPot, temperature)/(3*piSquared);
}

// returns quark results for given chemical potentials, temperature and bag constant
vector<double> quarkResults (double chemPotL, double chemPotQ, double temperature, double bag)
{
  double totalQuarkDensity = 0;
  double totalQuarkCharge = 0;
  double totalQuarkEntropy = 0;
  double totalQuarkEnergy = 0;
  double totalQuarkPressure = 0;
  double totalQuarkMu = 0;
  double quarkDensityIndividual[3];

  for( size_t ii = 0; ii < 3; ii++ ){

    //rmuq(i)=1.d0/3.d0*rmun-cq(i)*rmue
    double rmuq = (1.0/3)*chemPotQ - cquarks[ii]*chemPotL;

    struct densityParams params = {mquarks[ii], rmuq, temperature};

    double densityIntegralResult = solveIntegral(density, &params);

    totalQuarkCharge = totalQuarkCharge + 3*cquarks[ii]*densityIntegralResult;
    totalQuarkDensity = totalQuarkDensity + densityIntegralResult;
    totalQuarkMu = totalQuarkMu + 3*rmuq*densityIntegralResult;
    quarkDensityIndividual[ii] = densityIntegralResult;

    double energyIntegralResult = solveIntegral(energy, &params);

    totalQuarkEnergy = totalQuarkEnergy + 3.0*energyIntegralResult;

    double pressureIntegralResult = solveIntegral(pressure, &params);

    totalQuarkPressure = totalQuarkPressure + 3.0*pressureIntegralResult;

  }

  totalQuarkEnergy = totalQuarkEnergy + bag;
  totalQuarkPressure = totalQuarkPressure - bag;
  totalQuarkEntropy = (totalQuarkEnergy + totalQuarkPressure - totalQuarkMu)/ temperature;

  vector<double> quarkResultsVec;
  quarkResultsVec.push_back(totalQuarkCharge); // #0
  quarkResultsVec.push_back(totalQuarkDensity);
  quarkResultsVec.push_back(totalQuarkEnergy);
  quarkResultsVec.push_back(totalQuarkPressure); // #3
  quarkResultsVec.push_back(totalQuarkEntropy);
  quarkResultsVec.push_back(quarkDensityIndividual[0]); // #5
  quarkResultsVec.push_back(quarkDensityIndividual[1]);
  quarkResultsVec.push_back(quarkDensityIndividual[2]);

  return quarkResultsVec;
}

// returns total lepton charge, energy and pressure for given chemical potential and temperature
vector<double> leptonResults (double chemPotL, double temperature)
{
  double totalLeptonCharge = 0;
  double totalLeptonEnergy = 0;
  double totalLeptonPressure = 0;
  double totalLeptonMu = 0;
  double totalLeptonEntropy = 0;
  double leptonDensityIndividual[2];

  for( size_t ii = 0; ii < 2; ii++ ){

    struct densityParams params = {mleptons[ii], chemPotL, temperature};

    double densityIntegralResult = solveIntegral(density, &params);

    totalLeptonCharge = totalLeptonCharge + (-1)*densityIntegralResult;
    totalLeptonMu = totalLeptonMu + chemPotL*densityIntegralResult;
    leptonDensityIndividual[ii] = densityIntegralResult;

    double energyIntegralResult = solveIntegral(energy, &params);

    totalLeptonEnergy = totalLeptonEnergy + energyIntegralResult;

    double pressureIntegralResult = solveIntegral(pressure, &params);

    totalLeptonPressure = totalLeptonPressure + pressureIntegralResult;

  }

  totalLeptonEntropy = (totalLeptonEnergy + totalLeptonPressure - totalLeptonMu)/temperature;

  vector<double> leptonResultsVec;
  leptonResultsVec.push_back(totalLeptonCharge); // #0
  leptonResultsVec.push_back(totalLeptonEnergy);
  leptonResultsVec.push_back(totalLeptonPressure);
  leptonResultsVec.push_back(totalLeptonEntropy); // #3
  leptonResultsVec.push_back(leptonDensityIndividual[0]);
  leptonResultsVec.push_back(leptonDensityIndividual[1]);

  return leptonResultsVec;
}

// parameters of the system of equations
struct rootParams
  {
    double entropy;
    double baryonDensityIterParam;
    double bagParam;
  };

// describes system of equations to be solved
int rootEqs (const gsl_vector * x, void *params, gsl_vector * f)
{
  double entropy = ((struct rootParams *) params)->entropy;
  double iter = ((struct rootParams *) params)->baryonDensityIterParam;
  double bag = ((struct rootParams *) params)->bagParam;

  const double x0 = gsl_vector_get (x, 0);
  const double x1 = gsl_vector_get (x, 1);
  const double x2 = gsl_vector_get (x, 2);

  vector<double> qResults = quarkResults(x0, x1, x2, bag);
  vector<double> lResults = leptonResults(x0, x2);

  const double y0 = qResults[0] + lResults[0];
  const double y1 = qResults[1] - iter;
  const double y2 = iter*entropy - qResults[4] - lResults[3];

  gsl_vector_set (f, 0, y0);
  gsl_vector_set (f, 1, y1);
  gsl_vector_set (f, 2, y2);

  return GSL_SUCCESS;
}

// prints state of root solving function at each iteration
// This is meant for debugging only.
// If you want to print the iteration steps,
// uncomment "print_state" and "printf" lines within the solveSystem function
void print_state (size_t iter, gsl_multiroot_fsolver * s)
{
  cout << "iter = " << iter << '\t' << "x0 (mu_l)= " << '\t' << gsl_vector_get (s->x, 0) <<
    '\t' << "x1(mu_N) = " << gsl_vector_get (s->x, 1) << "x2 (T) = " << '\t' << gsl_vector_get (s->x, 2) << '\t' << 
    "f0 = " << gsl_vector_get (s->f, 0) << '\t' << "f1 = " << gsl_vector_get (s->f, 1) << '\t' <<
    "f2 = " << gsl_vector_get (s->f, 2) << endl;  
}

// solves the system
vector<double> solveSystem( double entropy, double baryonDensIter, double my_guess[3], double bag){

  const gsl_multiroot_fsolver_type *T;
  gsl_multiroot_fsolver *s;

  int status;
  size_t iter = 0;
  double successCounter = 0.0;

  const size_t n = 3;
  struct rootParams p = {entropy, baryonDensIter, bag};
  gsl_multiroot_function f = {&rootEqs, n, &p};

  double x_init[3] = {my_guess[0], my_guess[1], my_guess[2]};
  gsl_vector *x = gsl_vector_alloc (n);

  gsl_vector_set (x, 0, x_init[0]);
  gsl_vector_set (x, 1, x_init[1]);
  gsl_vector_set (x, 2, x_init[2]);

  T = gsl_multiroot_fsolver_hybrids;

  s = gsl_multiroot_fsolver_alloc (T, n);
  gsl_multiroot_fsolver_set (s, &f, x);

//  print_state (iter, s);

  do
    {
      iter++;
      status = gsl_multiroot_fsolver_iterate (s);

//      print_state (iter, s);

      if (status)   /* check if solver is stuck */
        break;

      status = 
        gsl_multiroot_test_residual (s->f, 1e-7);
    }
  while (status == GSL_CONTINUE && iter < 1000);

  if (strcmp(gsl_strerror(status), "success") != 0){
    successCounter = 1.0;
  }

  // printf ("status = %s\n", gsl_strerror (status));

  double muElectron = gsl_vector_get (s->x, 0);
  double muNeutron = gsl_vector_get (s->x, 1);
  double resultTemp = gsl_vector_get (s->x, 2);

  vector<double> systemResult;

  systemResult.push_back(muElectron);
  systemResult.push_back(muNeutron);
  systemResult.push_back(resultTemp);
  systemResult.push_back(successCounter);

  gsl_multiroot_fsolver_free (s);
  gsl_vector_free (x);

  return systemResult;
}

int main(int argc, char* argv[]) {

  // checks for correct usage of the program
  if (argc != 3){
    std::cerr << "Error: invalid input format. \n Correct program usage: \n ./programName entropy bagConst" << endl;
      return EXIT_FAILURE;
  }

  // reads entropy and bag constants
  // as inputs from user and applies required conversions
  double entropy = atof(argv[1]); // entropy = S/n_B 
  
  double bagctc = atof(argv[2]);
  bagctc = pow((bagctc/rm),4);

  // number of points to be calculated
  size_t nsteps = 300;
  size_t successCounterMain = 0;

  // initial guess for the system of equations
  double guess[3] = {0.2, 1.5, 0.02};

  // The looping index is the given total baryonic density
  double mainLoopIdx = 0.1/pow((rm/hc),3); 
  double stepSize = ((2.0 - 0.1)/nsteps)/pow((rm/hc),3);

  // prints useful information for the user
  cout << "Code running with parameters:" << '\n' << "entropy = " << argv[1] << 
    '\n' << "bag const = " << argv[2] << '\n' << endl;

  // Date and time of execution for logging
  std::time_t endTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
  string endTimeStamp = std::ctime(&endTime);
  endTimeStamp = endTimeStamp.substr(0,endTimeStamp.length()-1); // removes trailing newline

  // opens EoS output file and writes file header
  string eosFileName = "eos.S_"+string(argv[1])+".bag_"+string(argv[2])+".FD.dat";
  string outFileName = "out.S_"+string(argv[1])+".bag_"+string(argv[2])+".FD.dat";
  ofstream eosFileStream(eosFileName);
  ofstream outFileStream(outFileName);
  
  outFileStream << "# Output file for Mitbag Model with fixed Entropy and Fermi-Dirac statistics." << 
    '\n' << "# Ran on " << endTimeStamp << " with parameters: " << '\n' <<
    "# entropy = " << argv[1] << '\n' << "# bag const = " << argv[2] << 
    endl;
  
  outFileStream << "# ************************************************************" << '\n' << "#" << '\n' << "#" << endl;
  outFileStream << "Baryon_Dens" << '\t' << "Lep_Press" << '\t' << "Q_Press" << '\t' << "Tot_Press" << 
    '\t' << "Lep_Energy" << '\t' << "Q_Energy" << '\t' << "Tot_Energy" << '\t' << "Temperature" << '\t' <<
    "mu_electron" << '\t' << "mu_Neutron" << '\t' << "electron_fraction" << '\t' << "muon_fraction" <<
    '\t' << "up_q_fraction" << '\t' << "down_q_fraction" << '\t' << "strange_q_fraction" <<
    endl;

  // output in fixed format with 16 decimal digits
  eosFileStream.setf (ios::fixed, ios::floatfield);
  eosFileStream.precision (16);
  outFileStream.setf (ios::fixed, ios::floatfield);
  outFileStream.precision (16);
  
  // main loop
  while ( mainLoopIdx < 2.0/pow((rm/hc),3) ){

    // solves system
    vector<double> result = solveSystem(entropy, mainLoopIdx, guess, bagctc);

    double resultMuElectron = result[0];
    double resultMuNeutron = result[1];
    double resultTemp = result[2];

    // updates success counter if system converged to solution
    if(result[3] == 0.0){
      successCounterMain = successCounterMain + 1;
    }

    vector<double> leptonQuantities = leptonResults(resultMuElectron, resultTemp);

    double resultLeptonEnergy = leptonQuantities[1];
    double resultLeptonPressure = leptonQuantities[2];
    double electronFraction = leptonQuantities[4]/mainLoopIdx;
    double muonFraction = leptonQuantities[5]/mainLoopIdx;

    vector<double> quarkQuantities = quarkResults( resultMuElectron, resultMuNeutron, resultTemp, bagctc );

    double resultQuarkEnergy = quarkQuantities[2];
    double resultQuarkPressure = quarkQuantities[3];
    double upQuarkFraction = quarkQuantities[5]/mainLoopIdx;
    double downQuarkFraction = quarkQuantities[6]/mainLoopIdx;
    double strangeQuarkFraction = quarkQuantities[7]/mainLoopIdx;

    //calculates total Energy and Pressure
    double resultTotalEnergy = resultQuarkEnergy + resultLeptonEnergy;
    double resultTotalPressure = resultQuarkPressure + resultLeptonPressure;

    // writes results to files if conditions are met (convergence && Pressure > 0)
    if (resultTotalPressure > 0 && result[3] == 0.0) {
      eosFileStream << mainLoopIdx*pow((rm/hc),3) << '\t' << resultTotalEnergy*pow((rm/hc),4) << '\t' << 
        resultTotalPressure*pow((rm/hc),4) << '\t' << endl;

      outFileStream << mainLoopIdx*pow((rm/hc),3) << '\t' << resultLeptonPressure*pow((rm/hc),4) <<
        '\t' << resultQuarkPressure*pow((rm/hc),4) << '\t' << resultTotalPressure*pow((rm/hc),4) <<
        '\t' << resultLeptonEnergy*pow((rm/hc),4) << '\t' << resultQuarkEnergy*pow((rm/hc),4) <<
        '\t' << resultTotalEnergy*pow((rm/hc),4) << '\t' << resultTemp*rm << '\t' << resultMuElectron <<
        '\t' << resultMuNeutron << '\t' << electronFraction << '\t' << muonFraction <<
        '\t' << upQuarkFraction << '\t' << downQuarkFraction << '\t' << strangeQuarkFraction <<
        endl;
    }

    //updates loop
    mainLoopIdx = mainLoopIdx + stepSize;
    guess[0] = resultMuElectron;
    guess[1] = resultMuNeutron;
    guess[2] = resultTemp;   
  }

  // closes output files streams
  outFileStream.close();
  eosFileStream.close();

  // success message
  cout << "Done!" << '\n' << successCounterMain << " points succesfully converged for " << successCounterMain/nsteps*100 <<
    "% success rate." << '\n' << "Equation of State in fm-4 successfully saved to file " << eosFileName << '\n' <<
    "Full results saved to " << outFileName << 
    endl;

  return 0;

}