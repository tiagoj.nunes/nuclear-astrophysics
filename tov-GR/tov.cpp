#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint.hpp>
#include <boost/assign/std/vector.hpp>

#include "spline.h"

using namespace std;
using namespace boost::numeric::odeint;
using namespace boost::assign;

// defines requires constants and conversion constatants
const double pi = 3.14159265359;
const double CONV = 0.00026115; // 1 fm-4 = 2.6115e-4 km-2
const double CONVBARYON = 0.0012426726; // 939.0 * (1 MeV/fm3) = 939.0*1.3234e-6 km-2 = 1.2426726e-3 km-2
const double MSOL = 1.4766; // MSOL = 1.4766 km = 1.116e60 MeV

// type alias for vector state
typedef boost::array< double , 3 > state_type;

// declaration of spline objects
tk::spline energyDensitySpline;
tk::spline baryonDensitySpline;

// TOV equations
void tovEqs( const state_type &y , state_type &dydr , double r )
{
	dydr[0] = 4.0*pi*r*r*energyDensitySpline( y[1] ); // dm/dr
	dydr[1] = - ( energyDensitySpline(y[1]) + y[1] )*(y[0]+4*pi*r*r*r*y[1])/(r*r - 2*r*y[0]); // dpr/dr
	dydr[2] = baryonDensitySpline( y[1] )*4.0*pi*r*r/(sqrt(1-2*y[0]/r)); // d(baryonic density)/dr
}

// solves ODE using RK4 stepper and solver
vector<double> solveTOV( double r, const double dr, state_type &stateVector, const double prMIN )
{
	// stepper choice
	runge_kutta4< state_type > stepper;

	while (stateVector[1] > prMIN)
    {
    	r += dr;
    	stepper.do_step(tovEqs, stateVector, r, dr);
    }

    vector<double> odeResult;

    odeResult += r, stateVector[0]/MSOL, stateVector[1]/CONV, stateVector[2]/MSOL;

    return odeResult;
}

int main(int argc, char* argv[]) {

	// checks if program arguments are correct
	if (argc != 2){
		std::cerr << "Error: invalid input format. \n Correct program usage: \n ./programName eosFileName.dat" << endl;
    	return EXIT_FAILURE;
	}

	// integration variables
	double r = 0.0;
  double dr = 0.01;
	double dp = 0.01*CONV;

	// vector for storing maximum mass
  vector<double> maxMass(4, 0.0);

  // integration result vector
  vector<double> tovResult;

	// define vectors to store the contentes of EoS .dat file
	vector<double> baryonDensity;
	vector<double> energyDensity;
	vector<double> pressureR;

	// reads EoS .dat file into the vectors
	// Expected EoS file table format:
	// baryonDensity (fm^-4) '\t' energyDensity (fm^-4) '\t' pressure_r (fm^-4)
	ifstream infile(argv[1]);

	double col1, col2, col3;

	while (infile >> col1 >> col2 >> col3) {
		baryonDensity.push_back(col1);
		energyDensity.push_back(col2);
		pressureR.push_back(col3);
	}

	infile.close();

	// Converts from fm^-4 to km^-2
	transform(baryonDensity.begin(), baryonDensity.end(), baryonDensity.begin(), std::bind2nd(std::multiplies<double>(), CONVBARYON));
	transform(energyDensity.begin(), energyDensity.end(), energyDensity.begin(), std::bind2nd(std::multiplies<double>(), CONV));
	transform(pressureR.begin(), pressureR.end(), pressureR.begin(), std::bind2nd(std::multiplies<double>(), CONV));

	// initial values for pc and ec
	double maxPressure = pressureR[pressureR.size()-1];
	double pc = maxPressure;
	double ec = energyDensity[energyDensity.size()-1];
	double bc = baryonDensity[baryonDensity.size()-1];

	// sets spline up
	energyDensitySpline.set_points(pressureR, energyDensity);
	baryonDensitySpline.set_points(pressureR, baryonDensity);

	// creates stream to print results to file
	string massRadiusFileName = "massRadius."+string(argv[1]);
	ofstream massRadiusFileStream(massRadiusFileName);

  // output in fixed format with 16 decimal digits
  massRadiusFileStream.setf (ios::fixed, ios::floatfield);
  massRadiusFileStream.precision (16);

	// main loop for solving TOV equations
	while(pc > pressureR[0])
    {
        state_type y = {{4.0/3.0*pi*dr*dr*dr*ec, pc, 4.0/3.0*pi*dr*dr*dr*bc}};
        tovResult = solveTOV( r, dr, y, pressureR[0]);
        if (tovResult[1] > maxMass[1]){
        	maxMass[0] = tovResult[0];
        	maxMass[1] = tovResult[1];
        	maxMass[2] = tovResult[2];
        	maxMass[3] = tovResult[3];
        }
        massRadiusFileStream << tovResult[0] << '\t' << tovResult[1] << '\t' << tovResult[2] << '\t' << tovResult[3] << endl;
        pc = pc - dp;
        ec = energyDensitySpline( pc );
        bc = baryonDensitySpline( pc );
        cout << '\r' << round((1 - (pc - pressureR[0])/maxPressure)*100) << " " << '%' << " completed." << flush;

    }

    cout << '\n' << " R_MAX (Radius of Max Gravitational Mass (km)) = " << maxMass[0] << '\n' <<
    "Maximum Gravitational mass (/M_SOL) = " << maxMass[1] << '\n' <<
    "Baryonic Mass at R_MAX (/M_SOL) = " << maxMass[3] << '\n' <<
    "Gravitational Binding Energy (/M_SOL) = " << maxMass[3] - maxMass[1] << '\n' <<
    "Surface Pressure (fm-4) = " << maxMass[2] <<
    endl;

	return 0;
}
