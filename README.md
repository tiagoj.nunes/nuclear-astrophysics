# Nuclear Astrophysics

This repository contains nuclear astrophysics codes utilised in research projects in which I have been involved. 

Most of the codes contained here are written in C++ and might depend on the numerical Boost libraries for C++ and/or the GNU Scientific Library (GSL). Please refer to their websites for instructions on how to install them if the codes you are interested on need depend on those.

If you utilise any of the codes contained in this repository for your own work, I kindly request that you cite the respective original work.